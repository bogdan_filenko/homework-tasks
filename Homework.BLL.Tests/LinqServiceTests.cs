using Xunit;
using System;
using AutoMapper;
using Homework.DAL.Context;
using Homework.DAL.Entities;
using Homework.BLL.Services;
using Homework.Common.DTOs.Linq;
using Homework.BLL.MappingProfiles;
using Microsoft.EntityFrameworkCore;

namespace Homework.BLL.Tests
{
    public class LinqServiceTests : IDisposable
    {
        private readonly LinqService _linqService;
        private readonly ProjectsContext _db;
        private readonly IMapper _mapper;
        public LinqServiceTests()
        {
            var dbOptions = new DbContextOptionsBuilder<ProjectsContext>().UseInMemoryDatabase(databaseName: "ProjectsInMemory");

            _db = new ProjectsContext(dbOptions.Options);

            _mapper = new MapperConfiguration(config =>
            {
                config.AddProfile<LinqProfile>();
            }).CreateMapper();
            
            _linqService = new LinqService(_db, _mapper);
        }

        public void Dispose()
        {
            _db.Database.EnsureDeleted();
        }
        [Fact]
        public void GetFinishedTasks_WhenUserAndTasksExist_ThenSingle()
        {
            var user = new User()
            {
                FirstName = "John",
                LastName = "Smith",
                Email = "jSmith@gmail.com",
                RegisteredAt = DateTime.Now,
                BirthDate = DateTime.Parse("10/10/2002")
            };
            _db.Users.Add(user);
            _db.SaveChanges();

            _db.Tasks.AddRange(
                new Task() { Name = "Json serialization", State = 0, CreatedAt = DateTime.Now, PerformerId = user.Id },
                new Task() { Name = "Xml serialization", State = 2, CreatedAt = DateTime.Now, PerformerId = user.Id }
            );
            _db.SaveChanges();

            var finishedTasks = _linqService.GetFinishedTasks(user.Id).Result;

            Assert.Single(finishedTasks);
        }

        [Fact]
        public void GetTeamsWhereUsersOlderThan10_WhenSuchTeamsDoNotExist_ThenEmpty()
        {
            var team = new Team() { Name = "WWW", CreatedAt = DateTime.Now };
            _db.Teams.Add(team);
            _db.SaveChanges();
            
            var user = new User()
            {
                FirstName = "John",
                LastName = "Smith",
                Email = "jSmith@gmail.com",
                RegisteredAt = DateTime.Now,
                BirthDate = DateTime.Parse("10/10/2015"),
                TeamId = team.Id
            };
            _db.Users.Add(user);
            _db.SaveChanges();

            var teamsList = _linqService.GetTeamsWhereUsersOlderThan10().Result;

            Assert.Empty(teamsList);
        }

        [Fact]
        public void GetUserInfo_WhereUserHasNotTasksAndProjects_ThenUserInfo()
        {
            var user = new User()
            {
                FirstName = "John",
                LastName = "Smith",
                Email = "jSmith@gmail.com",
                RegisteredAt = DateTime.Now,
                BirthDate = DateTime.Parse("10/10/2002")
            };
            _db.Users.Add(user);
            _db.SaveChanges();
            
            var userInfo = _linqService.GetUserInfo(user.Id).Result;

            var expected = new UserInfoDTO() { User = _mapper.Map<UserLinqDTO>(user) };
            Assert.Equal(expected.User.Id, userInfo.User.Id);
            Assert.Equal(expected.LastProject, userInfo.LastProject);
            Assert.Equal(expected.LongestTask, userInfo.LongestTask);
            Assert.Equal(expected.LastProjectTotalTasks, userInfo.LastProjectTotalTasks);
            Assert.Equal(expected.TotalUnfinishedTasks, userInfo.TotalUnfinishedTasks);
        }
    }
}