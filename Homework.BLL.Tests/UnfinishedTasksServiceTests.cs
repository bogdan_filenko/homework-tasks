using Xunit;
using System;
using AutoMapper;
using Homework.DAL.Context;
using Homework.DAL.Entities;
using Homework.BLL.Services;
using Homework.BLL.Exceptions;
using Homework.BLL.MappingProfiles;
using Microsoft.EntityFrameworkCore;

namespace Homework.BLL.Tests
{
    public class UnfinishedTasksServiceTests : IDisposable
    {
        private readonly ProjectsContext _db;
        private readonly UnfinishedTasksService _unfinishedTasksService;

        public UnfinishedTasksServiceTests()
        {
            var dbOptions = new DbContextOptionsBuilder<ProjectsContext>().UseInMemoryDatabase(databaseName: "ProjectsInMemory");

            _db = new ProjectsContext(dbOptions.Options);

            IMapper mapper = new MapperConfiguration(config =>
            {
                config.AddProfile<TasksProfile>();
            }).CreateMapper();
            
            _unfinishedTasksService = new UnfinishedTasksService(_db, mapper);
        }

        public void Dispose()
        {
            _db.Database.EnsureDeleted();
        }

        [Fact]
        public void GetUnfinishedTasks_WhenUserExists_ThenTasksList()
        {
            var user = new User()
            { 
                FirstName = "John", 
                LastName = "Smith", 
                Email = "jSmith@gmail.com", 
                RegisteredAt = DateTime.Now, 
                BirthDate = DateTime.Parse("10/10/2002")
            };
            _db.Users.Add(user);
            _db.SaveChanges();
            
            _db.AddRange(
                new Task() { Name = "Json serialization", State = 2, CreatedAt = DateTime.Now, PerformerId = user.Id },
                new Task() { Name = "Xml serialization", State = 1, CreatedAt = DateTime.Now, PerformerId = user.Id },
                new Task() { Name = "Csv serialization", State = 2, CreatedAt = DateTime.Now, PerformerId = user.Id },
                new Task() { Name = "Blob parsing", State = 2, CreatedAt = DateTime.Now, PerformerId = user.Id }
            );
            _db.SaveChanges();

            var tasksList = _unfinishedTasksService.GetUnfinishedTasks(user.Id).Result;

            Assert.Single(tasksList);
        }

        [Fact]
        public void GetUnfinishedTasks_WhenUserDoesNotExist_ThenThrowNoEntityException()
        {
            Assert.ThrowsAsync<NoEntityException>(() => _unfinishedTasksService.GetUnfinishedTasks(100));
        }
    }
}