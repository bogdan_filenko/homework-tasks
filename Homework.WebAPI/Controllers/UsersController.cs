using System.Threading.Tasks;
using Homework.BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Homework.Common.DTOs.User;
using System.Collections.Generic;

namespace Homework.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService _usersService;
        public UsersController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDTO>>> Get()
        {
            return Ok(await _usersService.GetUsers());
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> Get(int id)
        {
            return Ok(await _usersService.GetUserById(id));
        }

        [HttpPost]
        public async Task<ActionResult<UserDTO>> Post([FromBody] CreateUserDTO dto)
        {
            return Ok(await _usersService.CreateUser(dto));
        }

        [HttpPut]
        public async Task<ActionResult<UserDTO>> Put([FromBody] UpdateUserDTO dto)
        {
            return Ok(await _usersService.UpdateUser(dto));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _usersService.DeleteUser(id);

            return NoContent();
        }
    }
}