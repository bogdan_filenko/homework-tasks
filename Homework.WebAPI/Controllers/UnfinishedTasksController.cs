using System.Threading.Tasks;
using Homework.BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Homework.Common.DTOs.Task;
using System.Collections.Generic;

namespace Homework.WebAPI.Controllers
{
    [ApiController]
    [Route("api/Tasks/")]
    public class UnfinishedTasksController : ControllerBase
    {
        private readonly IUnfinishedTasksService _unfinishedTasksService;
        public UnfinishedTasksController(IUnfinishedTasksService unfinishedTasksService)
        {
            _unfinishedTasksService = unfinishedTasksService;
        }

        [Route("User/{userId}/Unfinished")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetUnfinishedTasks(int userId)
        {
            return Ok(await _unfinishedTasksService.GetUnfinishedTasks(userId));
        }
    }
}