using Homework.WebAPI.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace Homework.WebAPI.Extensions
{
    public static class MiddlewareExtensions
    {
        public static void UseErrorsMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ErrorHandleMiddleware>();
        }
    }
}