using System;
using AutoMapper;
using System.Linq;
using Homework.DAL.Context;
using System.Threading.Tasks;
using Homework.BLL.Interfaces;
using Homework.Common.DTOs.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Homework.BLL.Services
{
    public sealed class LinqService : ILinqService
    {
        private readonly ProjectsContext _db;
        private readonly IMapper _mapper;

        private const int CurrentYear = 2021;
        private const int FinishedTaskCode = 2;

        public LinqService(ProjectsContext context, IMapper mapper)
        {
            _db = context;
            _mapper = mapper;
        }
        public async Task<IDictionary<ProjectLinqDTO, int>> GetProjectsTaskNumber(int userId)
        {
            var composedData = await _db.Projects
                .Include(p => p.Tasks)
                .Include(p => p.Author)
                .AsNoTracking()
                .ToListAsync();

            var mappedData = _mapper.Map<ProjectLinqDTO[]>(composedData);

            return mappedData
                .Where(p => p.Author.Id == userId)
                .ToDictionary(
                    project => project,
                    project => project.Tasks.Length);
        }

        public async Task<IEnumerable<TaskLinqDTO>> GetAllUserTasks(int userId)
        {
            var composedData = await _db.Projects
                .Include(p => p.Tasks)
                    .ThenInclude(t => t.Performer)
                .AsNoTracking()
                .ToListAsync();

            var mappedData = _mapper.Map<ProjectLinqDTO[]>(composedData);

            return mappedData
                .SelectMany(p => p.Tasks)
                .Where(t => t.Performer.Id == userId && t.Name.Length < 45);
        }

        public async Task<IEnumerable<ShortTaskDTO>> GetFinishedTasks(int userId)
        {
            var composedData = await _db.Projects
                .Include(p => p.Tasks)
                    .ThenInclude(t => t.Performer)
                .AsNoTracking()
                .ToListAsync();

            var mappedData = _mapper.Map<ProjectLinqDTO[]>(composedData); 

            return mappedData
                .SelectMany(p => p.Tasks)
                .Where(t => t.FinishedAt != default && t.FinishedAt.Value.Year == CurrentYear && t.Performer.Id == userId)
                .Select(t => _mapper.Map<ShortTaskDTO>(t));
        }
        
        public async Task<IEnumerable<ShortTeamDTO>> GetTeamsWhereUsersOlderThan10()
        {
            var composedData = await _db.Teams
                .Include(t => t.Members)
                .AsNoTracking()
                .ToListAsync();

            var mappedData = _mapper.Map<TeamLinqDTO[]>(composedData);

            return mappedData
                .Where(t => t.Members.All(u => DateTime.Now.Year - u.BirthDate.Year > 10))
                .Select(t =>
                {
                    var shortTeamDto = _mapper.Map<ShortTeamDTO>(t);
                    shortTeamDto.Members = t.Members.OrderByDescending(u => u.RegisteredAt).ToArray();

                    return shortTeamDto;
                });
        }
        
        public async Task<IEnumerable<UserLinqDTO>> GetUsersWithSortedTasks()
        {
            var composedData = await _db.Users
                .Include(u => u.Tasks)
                .AsNoTracking()
                .ToListAsync();

            var mappedData = _mapper.Map<UserLinqDTO[]>(composedData);

            return mappedData
                .Select(u =>
                {
                    u.Tasks = u.Tasks.OrderByDescending(t => t.Name.Length).ToArray();
                    return u;
                })
                .OrderBy(u => u.FirstName);
        }

        public async Task<UserInfoDTO> GetUserInfo(int userId)
        {
            var composedData = await _db.Users
                .Include(u => u.Tasks)
                .Include(u => u.Projects)
                .AsNoTracking()
                .ToListAsync();

            var mappedData = _mapper.Map<UserLinqDTO[]>(composedData);

            return mappedData
                .Where(u => u.Id == userId)
                .Select(u => new UserInfoDTO()
                {
                    User = u,
                    LastProject = u.Projects
                        ?.OrderByDescending(p => p.CreatedAt)
                        ?.FirstOrDefault() ?? null,
                    LastProjectTotalTasks = u.Projects
                        ?.OrderByDescending(p => p.CreatedAt)
                        ?.FirstOrDefault()?.Tasks?.Length ?? 0,
                    TotalUnfinishedTasks = u?.Tasks.Count(t => t.State != FinishedTaskCode) ?? 0,
                    LongestTask = u?.Tasks.OrderByDescending(t => t.FinishedAt - t.CreatedAt).FirstOrDefault() ?? null
                })
                .FirstOrDefault();
        }

        public async Task<IEnumerable<ProjectInfoDTO>> GetProjectsInfo()
        {
            var composedData = await _db.Projects
                .Include(p => p.Team)
                    .ThenInclude(t => t.Members)
                .Include(p => p.Tasks)
                .AsNoTracking()
                .ToListAsync();

            var mappedData = _mapper.Map<ProjectLinqDTO[]>(composedData);

            return mappedData
                .Where(p => p.Description.Length > 20 || p.Tasks.Length < 3)
                .Select(p => new ProjectInfoDTO()
                {
                    Project = p,
                    LongestByDescTask = p.Tasks
                        .OrderByDescending(t => t.Description)
                        .FirstOrDefault(),
                    ShortestByNameTask = p.Tasks
                        .OrderBy(t => t.Name)
                        .FirstOrDefault(),
                    TotalProjectParticians = p.Team.Members?.Length ?? 0
                });
        }
    }
}