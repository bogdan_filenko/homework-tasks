using System.Threading.Tasks;
using Homework.Common.DTOs.Linq;
using System.Collections.Generic;

namespace Homework.BLL.Interfaces
{
    public interface ILinqService
    {
        Task<IDictionary<ProjectLinqDTO, int>> GetProjectsTaskNumber(int userId);

        Task<IEnumerable<TaskLinqDTO>> GetAllUserTasks(int userId);

        Task<IEnumerable<ShortTaskDTO>> GetFinishedTasks(int userId);
        
        Task<IEnumerable<ShortTeamDTO>> GetTeamsWhereUsersOlderThan10();
        
        Task<IEnumerable<UserLinqDTO>> GetUsersWithSortedTasks();

        Task<UserInfoDTO> GetUserInfo(int userId);

        Task<IEnumerable<ProjectInfoDTO>> GetProjectsInfo();
    }
}