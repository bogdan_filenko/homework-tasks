using AutoMapper;
using Homework.DAL.Entities;
using Homework.Common.DTOs.Linq;

namespace Homework.BLL.MappingProfiles
{
    public sealed class LinqProfile : Profile
    {
        public LinqProfile()
        {
            CreateMap<Project, ProjectLinqDTO>();

            CreateMap<User, UserLinqDTO>();

            CreateMap<Team, TeamLinqDTO>();

            CreateMap<Task, TaskLinqDTO>();

            CreateMap<TaskLinqDTO, ShortTaskDTO>();

            CreateMap<TeamLinqDTO, ShortTeamDTO>();
        }
    }
}