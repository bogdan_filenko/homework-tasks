using System;

namespace Homework.BLL.Exceptions
{
    public sealed class NoEntityException : Exception
    {
        public NoEntityException(Type type, int id)
            : base($"An entity with type {type} and id {id} does not exist")
        { }
    }
}