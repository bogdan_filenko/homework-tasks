namespace Homework.Client.Models
{
    public enum TaskState : byte
    {
        Added,
        InProcess,
        Finished,
        Refused
    }
}