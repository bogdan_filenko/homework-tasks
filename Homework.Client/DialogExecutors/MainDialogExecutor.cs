using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Homework.Client.Services;
using Homework.Common.DTOs.Linq;
using Homework.Client.Interfaces;
using System.Collections.Generic;
using Homework.Client.DialogExecutors.Abstract;

namespace Homework.Client.DialogExecutors
{
    public sealed class MainDialogExecutor : DialogExecutor
    {
        private const int TaskMarkerDelayMilliseconds = 1000;
        private static ITaskMarkerService _taskMarkerService;
        private readonly CancellationTokenSource _cts;

        private bool _isTaskMarkerVisible = false;

        public MainDialogExecutor(Cui cui) 
            : base(cui)
        {
            _cts = new CancellationTokenSource();
        }

        public override async Task Execute()
        {
            Task taskMarkerTask = ExecuteTasksMarker();

            while(_isOpened)
            {
                try
                {
                    _cui.ShowMainDialog(_isTaskMarkerVisible);
                    char option = Console.ReadKey(true).KeyChar;

                    switch(option)
                    {
                        case '1':
                        {
                            int? userId = ExecuteUserIdDialog();
                            if (userId != default)
                            {
                                var response = await _httpService.Get<IEnumerable<KeyValuePair<ProjectLinqDTO, int>>>($"/Linq/users/{userId}/projects/tasks");
                                await _dataViewer.ViewProjectsTaskNumber(response.ToDictionary(kv => kv.Key, kv => kv.Value));
                            }
                            
                            ResetScreen();
                            break;
                        }
                        case '2':
                        {
                            int? userId = ExecuteUserIdDialog();
                            if (userId != default)
                            {
                                var response = await _httpService.Get<IEnumerable<TaskLinqDTO>>($"/Linq/users/{userId}/tasks");
                                await _dataViewer.ViewCollectionData<TaskLinqDTO>(response);
                            }

                            ResetScreen();
                            break;
                        }
                        case '3':
                        {
                            int? userId = ExecuteUserIdDialog();
                            if (userId != default)
                            {
                                var response = await _httpService.Get<IEnumerable<ShortTaskDTO>>($"/Linq/users/{userId}/tasks/finished");
                                await _dataViewer.ViewCollectionData<ShortTaskDTO>(response);
                            }

                            ResetScreen();
                            break;
                        }
                        case '4':
                        {
                            var response = await _httpService.Get<IEnumerable<ShortTeamDTO>>("/Linq/teams/users/ordered");
                            await _dataViewer.ViewCollectionData<ShortTeamDTO>(response);
                            
                            ResetScreen();
                            break;
                        }
                        case '5':
                        {
                            var response = await _httpService.Get<IEnumerable<UserLinqDTO>>("/Linq/users/tasks/ordered");
                            await _dataViewer.ViewCollectionData<UserLinqDTO>(response);

                            ResetScreen();
                            break;
                        }
                        case '6':
                        {
                            int? userId = ExecuteUserIdDialog();
                            if (userId != default)
                            {
                                var response = await _httpService.Get<UserInfoDTO>($"/Linq/users/{userId}/info");
                                _dataViewer.ViewSingleEntity<UserInfoDTO>(response);
                            }

                            ResetScreen();
                            break;
                        }
                        case '7':
                        {
                            var response = await _httpService.Get<IEnumerable<ProjectInfoDTO>>("/Linq/projects/info");
                            await _dataViewer.ViewCollectionData<ProjectInfoDTO>(response);

                            ResetScreen();
                            break;
                        }
                        case '8':
                        {
                            await new OthersDialogExecutor(_cui).Execute();
                            break;
                        }
                        case '9':
                        {
                            _isTaskMarkerVisible = _isTaskMarkerVisible
                                ? false
                                : true;

                            break;
                        }
                        case '0':
                        {
                            _cts.Cancel();
                            await taskMarkerTask;

                            _cui.ShowMessage("Program is closing");
                            _isOpened = false;
                            break;
                        }
                        default:
                        {
                            _cui.ShowMessage("Unknown option");
                            ResetScreen();
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    _cui.ShowMessage(ex.Message);
                    ResetScreen();
                }
            }
        }

        public int? ExecuteUserIdDialog()
        {
            _cui.ShowUserIdDialog();
            if (!int.TryParse(Console.ReadLine(), out int userId))
            {
                _cui.ShowMessage("Invalid user`s id format");
                return default;
            }
            return userId;
        }

        private async Task ExecuteTasksMarker()
        {
            using (_taskMarkerService = TaskMarkerService.GetInstance())
            while (!_cts.IsCancellationRequested)
            {
                try
                {
                    var markedTaskId = await _taskMarkerService.MarkRandomTaskWithDelay(TaskMarkerDelayMilliseconds);
                    if (_isTaskMarkerVisible)
                    {
                        _cui.ShowMessage($"Marked task id: {markedTaskId}");
                    }
                }
                catch (AggregateException ex)
                {
                    if (_isTaskMarkerVisible)
                    {
                        _cui.ShowMessage(ex.Message);
                    }
                }
            }
            _cui.ShowMessage("Marker task is closing");
        }
    }
}