using System;
using System.Linq;
using System.Timers;
using Homework.Client.Models;
using System.Threading.Tasks;
using Homework.Common.DTOs.Task;
using System.Collections.Generic;
using Homework.Client.Interfaces;

namespace Homework.Client.Services
{
    public sealed class TaskMarkerService : ITaskMarkerService
    {
        private static TaskMarkerService _taskMarkerService;

        private readonly Timer _timer;
        private readonly IHttpService _httpService;

        public static TaskMarkerService GetInstance()
        {
            if (_taskMarkerService == default)
            {
                return new TaskMarkerService();
            }

            return _taskMarkerService;
        }
        
        private TaskMarkerService()
        {
            _timer = new Timer();
            _timer.AutoReset = false;

            _httpService = new HttpService();
        }
        
        public Task<int> MarkRandomTaskWithDelay(int delayMilliseconds)
        {
            TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();

            ElapsedEventHandler callback = null;
            callback = async (o, e) =>
            {
                try
                {
                    var tasks = await _httpService.Get<IEnumerable<TaskDTO>>("/Tasks");
                    
                    Random randomGen = new Random();
                    var randomTask = tasks.ElementAt(randomGen.Next(tasks.Count()));

                    await _httpService.Put<UpdateTaskDTO, TaskDTO>("/Tasks", new UpdateTaskDTO
                    {
                        Id = randomTask.Id,
                        Name = randomTask.Name,
                        Description = randomTask.Description,
                        State = (int)TaskState.Finished,
                        PerformerId = randomTask.PerformerId
                    });

                    tcs.SetResult(randomTask.Id);
                }
                catch (Exception ex)
                {
                    tcs.SetException(new AggregateException(ex));
                }
                finally
                {
                    _timer.Elapsed -= callback;
                }
            };

            _timer.Elapsed += callback;
            _timer.Interval = delayMilliseconds;
            _timer.Enabled = true;
            
            return tcs.Task;
        }

        public void Dispose()
        {
            _timer.Stop();
            _timer.Dispose();

            _httpService.Dispose();
        }
    }
}