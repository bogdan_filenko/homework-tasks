using System;
using System.Linq;
using System.Threading.Tasks;
using Homework.Common.DTOs.Linq;
using System.Collections.Generic;

namespace Homework.Client
{
    public sealed class ResponseDataViewer
    {
        public async Task ViewProjectsTaskNumber(IDictionary<ProjectLinqDTO, int> responseData)
        {
            Console.Clear();

            if (responseData != default)
            {
                await Task.Run(() =>
                {
                    var stringifiedData = responseData.AsParallel()
                        .AsOrdered()
                        .Select(t => t.Key.ToString() + '\n' +
                            $"Tasks` number: {t.Value}\n");
                    
                    foreach (var data in stringifiedData)
                    {
                        Console.WriteLine(data);
                    }
                });
            }
        }
        public async Task ViewCollectionData<T>(IEnumerable<T> responseData)
        {
            Console.Clear();

            if (responseData != default)
            {
                await Task.Run(() =>
                {
                    var stringifiedData = responseData.AsParallel()
                        .AsOrdered()
                        .Select(e => e.ToString() + '\n');

                    foreach (var data in stringifiedData)
                    {
                        Console.WriteLine(data);
                    }
                });
            }
        }
        public void ViewSingleEntity<T>(T responseData)
        {
            Console.Clear();

            if (responseData != null)
            {
                Console.WriteLine(responseData.ToString() + '\n');
            }
        }
    }
}