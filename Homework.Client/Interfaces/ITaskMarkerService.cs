using System;
using System.Threading.Tasks;

namespace Homework.Client.Interfaces
{
    public interface ITaskMarkerService : IDisposable
    {
        Task<int> MarkRandomTaskWithDelay(int delayMilliseconds);
    }
}