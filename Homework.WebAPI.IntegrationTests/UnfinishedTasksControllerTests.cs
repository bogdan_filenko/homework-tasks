using Xunit;
using System;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Homework.Common.DTOs.User;
using Homework.Common.DTOs.Task;

namespace Homework.WebAPI.IntegrationTests
{
    public class UnfinishedTasksControllerTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        public UnfinishedTasksControllerTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task GetUnfinishedTasks_WhenUserExists_ThenTasksList()
        {
            var createUser = new CreateUserDTO()
            {
                FirstName = "John",
                LastName = "Smith",
                Email = "jSmith@gmail.com",
                BirthDate = DateTime.Parse("10/10/2002")
            };
            var content = new StringContent(JsonConvert.SerializeObject(createUser), Encoding.UTF8, "application/json");
            
            var httpResponse = await _client.PostAsync("api/Users", content);
            var createdUser = JsonConvert.DeserializeObject<UserDTO>(await httpResponse.Content.ReadAsStringAsync());

            httpResponse = await _client.GetAsync($"api/Tasks/User/{createdUser.Id}/Unfinished");

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Empty(JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(await httpResponse.Content.ReadAsStringAsync()));
        }

        [Fact]
        public async Task GetUnfinishedTasks_WhenUserDoesNotExist_ThenNotFound()
        {
            var httpResponse = await _client.GetAsync("api/Tasks/User/100/Unfinished");

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }
    }
}