using Xunit;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using Homework.Common.DTOs.Team;

namespace Homework.WebAPI.IntegrationTests
{
    public class TeamsControllerTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public TeamsControllerTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Theory]
        [InlineData("WWW")]
        [InlineData("Happy meal")]
        [InlineData("Rolecoasters")]
        public async Task CreateTeam_WhereTeamIsCorrect_ThenCreatedTeam(string? name)
        {
            var createTeam = new CreateTeamDTO() { Name = name };
            var content = new StringContent(JsonConvert.SerializeObject(createTeam), Encoding.UTF8, "application/json");

            var httpResponse = await _client.PostAsync("api/Teams", content);
            var createdTeam = JsonConvert.DeserializeObject<TeamDTO>(await httpResponse.Content.ReadAsStringAsync());

            await _client.DeleteAsync($"api/Teams/{createdTeam.Id}");

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.NotNull(createTeam);
        }

        [Fact]
        public async Task CreateTeam_WhereTeamExists_ThenBadRequest()
        {
            var createTeam = new CreateTeamDTO() { Name = "TTT" };
            var content = new StringContent(JsonConvert.SerializeObject(createTeam), Encoding.UTF8, "application/json");
            
            var createdTeamJson = await (await _client.PostAsync("api/Teams", content)).Content.ReadAsStringAsync();
            var createdTeam = JsonConvert.DeserializeObject<TeamDTO>(createdTeamJson);

            var httpResponse = await _client.PostAsync("api/Teams", content);

            await _client.DeleteAsync($"api/Teams/{createdTeam.Id}");

            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }
    }
}