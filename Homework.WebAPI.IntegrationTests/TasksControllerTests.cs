using Xunit;
using System;
using System.Net;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Homework.Common.DTOs.User;
using Homework.Common.DTOs.Task;
using Homework.Common.DTOs.Team;
using Homework.Common.DTOs.Project;

namespace Homework.WebAPI.IntegrationTests
{
    public class TasksControllerTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public TasksControllerTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task DeleteTask_WhenTaskExists_ThenNoContent()
        {
            var createTeam = new CreateTeamDTO() { Name = "WWW" };
            var createUser = new CreateUserDTO() {
                FirstName = "John",
                LastName = "Smith",
                Email = "jSmith@gmail.com",
                BirthDate = DateTime.Parse("10/10/1995")
            };
            
            var httpResponse = await _client.PostAsync("api/Teams", new StringContent(JsonConvert.SerializeObject(createTeam), Encoding.UTF8, "application/json"));
            var team = JsonConvert.DeserializeObject<TeamDTO>(await httpResponse.Content.ReadAsStringAsync());

            httpResponse = await _client.PostAsync("api/Users", new StringContent(JsonConvert.SerializeObject(createUser), Encoding.UTF8, "application/json"));
            var user = JsonConvert.DeserializeObject<UserDTO>(await httpResponse.Content.ReadAsStringAsync());

            var createProject = new CreateProjectDTO()
            {
                Name = "Rolecoaster",
                Deadline = DateTime.Now.AddMonths(5),
                AuthorId = user.Id,
                TeamId = team.Id
            };

            httpResponse = await _client.PostAsync("api/Projects", new StringContent(JsonConvert.SerializeObject(createProject), Encoding.UTF8, "application/json"));
            var project = JsonConvert.DeserializeObject<ProjectDTO>(await httpResponse.Content.ReadAsStringAsync());

            var createTask = new CreateTaskDTO()
            {
                Name = "Json serialization",
                ProjectId = project.Id,
                PerformerId = user.Id
            };

            httpResponse = await _client.PostAsync("api/Tasks", new StringContent(JsonConvert.SerializeObject(createTask), Encoding.UTF8, "application/json"));
            var task = JsonConvert.DeserializeObject<TaskDTO>(await httpResponse.Content.ReadAsStringAsync());

            httpResponse = await _client.DeleteAsync($"api/Tasks/{task.Id}");

            await _client.DeleteAsync($"api/Projects/{project.Id}");
            await _client.DeleteAsync($"api/Users/{user.Id}");
            await _client.DeleteAsync($"api/Teams/{team.Id}");

            Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);
        }

        [Fact]
        public async Task DeleteTask_WhenTaskDoesNotExist_ThenNotFound()
        {
            var httpResponse = await _client.DeleteAsync("api/Tasks/1000");

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }
    }
}